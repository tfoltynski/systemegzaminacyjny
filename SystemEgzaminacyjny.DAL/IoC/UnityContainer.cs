﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SystemEgzaminacyjny.DAL.DbModel;
using SystemEgzaminacyjny.DAL.Repositories;
using SystemEgzaminacyjny.DAL.Repositories.Interfaces;

namespace SystemEgzaminacyjny.DAL.IoC
{
	public class UnityContainer
	{
		#region Unity Container
		private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
		{
			var container = new Microsoft.Practices.Unity.UnityContainer();
			RegisterTypes(container);
			return container;
		});

		/// <summary>
		/// Gets the configured Unity container.
		/// </summary>
		public static IUnityContainer GetConfiguredContainer()
		{
			return container.Value;
		}
		#endregion

		/// <summary>Registers the type mappings with the Unity container.</summary>
		/// <param name="container">The unity container to configure.</param>
		/// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
		/// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
		private static void RegisterTypes(IUnityContainer container)
		{
			// NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
			// container.LoadConfiguration();
			container.RegisterType<SystemEgzaminacyjnyDbContext>();
			container.RegisterTypes(AllClasses.FromAssemblies(typeof(AnswerRepository).Assembly), WithMappings.FromMatchingInterface, WithName.Default);
			// TODO: Register your types here
			// container.RegisterType<IProductRepository, ProductRepository>();

			//container.RegisterType<IRoleStore<IdentityRole, string>, RoleStore<IdentityRole>>();
			//container.RegisterType<RoleManager<IdentityRole>, ApplicationRoleManager>();
		}
	}
}