﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SystemEgzaminacyjny.DAL.Entities
{
	public class Exam
	{
		public Exam()
		{
			Questions = new List<Question>();
			Enrollments = new List<Enrollment>();
		}

		public int Id { get; set; }
		[Required]
		public int Duration { get; set; }
		[Required]
		public string Topic { get; set; }

		public virtual ICollection<Question> Questions { get; set; }
		public virtual ICollection<Enrollment> Enrollments { get; set; }
	}
}