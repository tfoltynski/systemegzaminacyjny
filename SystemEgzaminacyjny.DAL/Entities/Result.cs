﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SystemEgzaminacyjny.DAL.DbModel;

namespace SystemEgzaminacyjny.DAL.Entities
{
	public class Result
	{
		public int Id { get; set; }
		public string UserId { get; set; }
		public int EnrollmentId { get; set; }
		public int Score { get; set; }

		public virtual ApplicationUser Student { get; set; }
		public virtual Enrollment Enrollment { get; set; }
	}
}
