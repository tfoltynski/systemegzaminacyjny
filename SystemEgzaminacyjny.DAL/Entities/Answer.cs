﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SystemEgzaminacyjny.DAL.Entities
{
	public class Answer
	{
		public int Id { get; set; }
		public int QuestionId { get; set; }
		[Required]
		public string Content { get; set; }
		public bool Correct { get; set; }

		public virtual Question Question { get; set; }
	}
}