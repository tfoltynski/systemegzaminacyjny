﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemEgzaminacyjny.DAL.Entities
{
	public class RandomQuestion
	{
		public RandomQuestion()
		{
			UserAnswers = new List<UserAnswer>();
		}

		public int Id { get; set; }
		public int QuestionId { get; set; }
		public int EnrollmentId { get; set; }
		public bool IsAnswered { get; set; }

		public virtual ICollection<UserAnswer> UserAnswers { get; set; }
	}
}