﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SystemEgzaminacyjny.DAL.Entities
{
	public class Question
	{
		public Question()
		{
			Answers = new List<Answer>();
		}

		public int Id { get; set; }
		public int ExamId { get; set; }
		[Required]
		public string Content { get; set; }
		public string ImagePath { get; set; }

		public virtual ICollection<Answer> Answers { get; set; }
		public virtual Exam Exam { get; set; }
	}
}