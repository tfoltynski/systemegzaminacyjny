﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SystemEgzaminacyjny.DAL.DbModel;

namespace SystemEgzaminacyjny.DAL.Entities
{
	public class Enrollment
	{
		public Enrollment()
		{
			RandomQuestions = new List<RandomQuestion>();
		}

		public int Id { get; set; }
		public int ExamId { get; set; }
		public string UserId { get; set; }
		public DateTime? ExamDate { get; set; }
		public DateTime EnrollDate { get; set; }
		public float Score { get; set; }
		public int NumberOfQuestions { get; set; }
		public int PassThreshold { get; set; }
		public bool IsExamFinished { get; set; }
		public bool IsExamInProgress { get; set; }

		public virtual ApplicationUser User { get; set; }
		public virtual Exam Exam { get; set; }
		public virtual ICollection<RandomQuestion> RandomQuestions { get; set; }
	}
}