﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SystemEgzaminacyjny.DAL.DbModel;
using SystemEgzaminacyjny.DAL.Entities;
using SystemEgzaminacyjny.DAL.Repositories.Base;
using SystemEgzaminacyjny.DAL.Repositories.Interfaces;

namespace SystemEgzaminacyjny.DAL.Repositories
{
	public class AnswerRepository : BaseRepository<Answer>, IAnswerRepository
	{
		private readonly SystemEgzaminacyjnyDbContext _dbContext;

		public AnswerRepository(SystemEgzaminacyjnyDbContext dbContext)
			: base(dbContext)
		{
			_dbContext = dbContext;
		}

		public List<Answer> GetByQuestionId(int questionId)
		{
			return _dbContext.Answer.Where(p => p.QuestionId == questionId).ToList();
		}
	}
}