﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace SystemEgzaminacyjny.DAL.Repositories.Base
{
	public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
	{
		protected readonly DbContext DbContext;

		protected virtual DbSet<TEntity> Table { get { return DbContext.Set<TEntity>(); } }

		public BaseRepository(DbContext dbContext)
		{
			DbContext = dbContext;
		}

		public TEntity GetSingle(Expression<Func<TEntity, bool>> predicate)
		{
			return Table.FirstOrDefault(predicate);
		}

		public virtual IQueryable<TEntity> GetAll()
		{
			return Table.AsQueryable();
		}

		public IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate)
		{
			return Table.Where(predicate);
		}

		public virtual void Add(TEntity entity)
		{
			Table.Add(entity);
		}

		public virtual void Update(TEntity entity)
		{
			Table.Attach(entity);
			DbContext.Entry(entity).State = EntityState.Modified;
		}

		public virtual void Delete(TEntity entity)
		{
			Table.Remove(entity);
		}

		public void Save()
		{
			DbContext.SaveChanges();
		}

		private bool disposed = false;

		protected virtual void Dispose(bool disposing)
		{

			if (!this.disposed)
				if (disposing)
					DbContext.Dispose();

			this.disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}