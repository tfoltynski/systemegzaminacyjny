﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace SystemEgzaminacyjny.DAL.Repositories.Base
{
	public interface IBaseRepository<TEntity> : IDisposable where TEntity : class
	{
		TEntity GetSingle(Expression<Func<TEntity, bool>> predicate);
		IQueryable<TEntity> GetAll();
		IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate);
		void Add(TEntity entity);
		void Update(TEntity entity);
		void Delete(TEntity entity);
		void Save();
	}
}