﻿using System.Collections.Generic;
using System.Linq;
using SystemEgzaminacyjny.DAL.DbModel;
using SystemEgzaminacyjny.DAL.Entities;
using SystemEgzaminacyjny.DAL.Repositories.Base;
using SystemEgzaminacyjny.DAL.Repositories.Interfaces;

namespace SystemEgzaminacyjny.DAL.Repositories
{
	public class QuestionRepository : BaseRepository<Question>, IQuestionRepository
	{
		private readonly SystemEgzaminacyjnyDbContext _dbContext;

		public QuestionRepository(SystemEgzaminacyjnyDbContext dbContext)
			: base(dbContext)
		{
			_dbContext = dbContext;
		}

		public List<Question> GetByExamId(int examId)
		{
			return _dbContext.Question.Where(p => p.ExamId == examId).ToList();
		}
	}
}