﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SystemEgzaminacyjny.DAL.DbModel;
using SystemEgzaminacyjny.DAL.Entities;
using SystemEgzaminacyjny.DAL.Repositories.Base;
using SystemEgzaminacyjny.DAL.Repositories.Interfaces;

namespace SystemEgzaminacyjny.DAL.Repositories
{
	public class RandomQuestionRepository : BaseRepository<RandomQuestion>, IRandomQuestionRepository
	{
		private readonly SystemEgzaminacyjnyDbContext _dbContext;

		public RandomQuestionRepository(SystemEgzaminacyjnyDbContext dbContext)
			: base(dbContext)
		{
			_dbContext = dbContext;
		}

		public RandomQuestion GetQuestionForEnrollment(int enrollmentId)
		{
			return _dbContext.RandomQuestion.FirstOrDefault(p => p.EnrollmentId == enrollmentId && p.IsAnswered == false);
		}

		public int RemainingQuestions(int enrollmentId)
		{
			return _dbContext.RandomQuestion.Where(p => p.EnrollmentId == enrollmentId && p.IsAnswered == false).Count();
		}
	}
}