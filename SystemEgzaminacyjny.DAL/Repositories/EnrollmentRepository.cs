﻿using System;
using System.Data.Entity;
using System.Linq;
using SystemEgzaminacyjny.DAL.DbModel;
using SystemEgzaminacyjny.DAL.Entities;
using SystemEgzaminacyjny.DAL.Repositories.Base;
using SystemEgzaminacyjny.DAL.Repositories.Interfaces;

namespace SystemEgzaminacyjny.DAL.Repositories
{
	public class EnrollmentRepository : BaseRepository<Enrollment>, IEnrollmentRepository
	{
		private readonly SystemEgzaminacyjnyDbContext _dbContext;

		public EnrollmentRepository(SystemEgzaminacyjnyDbContext dbContext) : base(dbContext)
		{
			_dbContext = dbContext;
		}

		public void Update(int id, DateTime? examDate)
		{
			var enrollment = new Enrollment()
			{
				Id = id,
				ExamDate = examDate
			};

			_dbContext.Enrollment.Attach(enrollment);
			_dbContext.Entry(enrollment).Property(p => p.ExamDate).IsModified = true;
			_dbContext.SaveChanges();
		}
	}
}