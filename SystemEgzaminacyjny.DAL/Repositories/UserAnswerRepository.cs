﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SystemEgzaminacyjny.DAL.DbModel;
using SystemEgzaminacyjny.DAL.Entities;
using SystemEgzaminacyjny.DAL.Repositories.Base;
using SystemEgzaminacyjny.DAL.Repositories.Interfaces;

namespace SystemEgzaminacyjny.DAL.Repositories
{
	public class UserAnswerRepository : BaseRepository<UserAnswer>, IUserAnswerRepository
	{
		private readonly SystemEgzaminacyjnyDbContext _dbContext;

		public UserAnswerRepository(SystemEgzaminacyjnyDbContext dbContext)
			: base(dbContext)
		{
			_dbContext = dbContext;
		}
	}
}