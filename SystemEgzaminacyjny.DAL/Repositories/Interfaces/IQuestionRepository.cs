﻿using System.Collections.Generic;
using System.Linq;
using SystemEgzaminacyjny.DAL.Entities;
using SystemEgzaminacyjny.DAL.Repositories.Base;

namespace SystemEgzaminacyjny.DAL.Repositories.Interfaces
{
	public interface IQuestionRepository : IBaseRepository<Question>
	{
		List<Question> GetByExamId(int examId);
	}
}
