﻿using System.Collections.Generic;
using SystemEgzaminacyjny.DAL.Entities;
using SystemEgzaminacyjny.DAL.Repositories.Base;

namespace SystemEgzaminacyjny.DAL.Repositories.Interfaces
{
	public interface IRandomQuestionRepository : IBaseRepository<RandomQuestion>
	{
		RandomQuestion GetQuestionForEnrollment(int enrollmentId);
		int RemainingQuestions(int enrollmentId);
	}
}
