﻿using System.Collections.Generic;
using System.Linq;
using SystemEgzaminacyjny.DAL.Entities;
using SystemEgzaminacyjny.DAL.Repositories.Base;

namespace SystemEgzaminacyjny.DAL.Repositories.Interfaces
{
	public interface IAnswerRepository : IBaseRepository<Answer>
	{
		List<Answer> GetByQuestionId(int questionId);
	}
}
