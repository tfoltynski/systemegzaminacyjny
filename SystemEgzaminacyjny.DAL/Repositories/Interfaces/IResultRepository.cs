﻿using System.Linq;
using SystemEgzaminacyjny.DAL.Entities;
using SystemEgzaminacyjny.DAL.Repositories.Base;

namespace SystemEgzaminacyjny.DAL.Repositories.Interfaces
{
	public interface IResultRepository : IBaseRepository<Result>
	{
	}
}
