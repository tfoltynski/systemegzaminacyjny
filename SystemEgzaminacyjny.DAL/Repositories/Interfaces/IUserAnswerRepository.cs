﻿using SystemEgzaminacyjny.DAL.Entities;
using SystemEgzaminacyjny.DAL.Repositories.Base;

namespace SystemEgzaminacyjny.DAL.Repositories.Interfaces
{
	public interface IUserAnswerRepository : IBaseRepository<UserAnswer>
	{
	}
}
