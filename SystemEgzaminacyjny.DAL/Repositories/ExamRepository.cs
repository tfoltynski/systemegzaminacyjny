﻿using System.Linq;
using SystemEgzaminacyjny.DAL.DbModel;
using SystemEgzaminacyjny.DAL.Entities;
using SystemEgzaminacyjny.DAL.Repositories.Base;
using SystemEgzaminacyjny.DAL.Repositories.Interfaces;

namespace SystemEgzaminacyjny.DAL.Repositories
{
	public class ExamRepository : BaseRepository<Exam>, IExamRepository
	{
		private readonly SystemEgzaminacyjnyDbContext _dbContext;

		public ExamRepository(SystemEgzaminacyjnyDbContext dbContext) : base(dbContext)
		{
			_dbContext = dbContext;
		}
	}
}