﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using SystemEgzaminacyjny.DAL.Entities;

namespace SystemEgzaminacyjny.DAL.DbModel
{
	public class SystemEgzaminacyjnyDbContext : IdentityDbContext<ApplicationUser>
	{
		public SystemEgzaminacyjnyDbContext() : base("DefaultConnection")
		{
			Database.SetInitializer<SystemEgzaminacyjnyDbContext>(new DropCreateIfChangeInitializer());
		}

		public DbSet<Exam> Exam { get; set; }
		public DbSet<Enrollment> Enrollment { get; set; }
		public DbSet<Question> Question { get; set; }
		public DbSet<Answer> Answer { get; set; }
		public DbSet<Result> Result { get; set; }
		public DbSet<RandomQuestion> RandomQuestion { get; set; }
		public DbSet<UserAnswer> UserAnswer { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}

		public void Seed(SystemEgzaminacyjnyDbContext context)
		{
			new List<Exam>
			{
				new Exam { Id = 1, Duration = 30, Topic = "ASP .NET MVC Test" }
			}.ForEach(i => context.Exam.Add(i));

			new List<Question>
			{
				new Question { Id = 1, ExamId = 1, Content = "Co oznacza skrót MVC?" },
				new Question { Id = 2, ExamId = 1, Content = "Jaka jest różnica między TempData a ViewData?"}
			}.ForEach(i => context.Question.Add(i));

			new List<Answer>
			{
				new Answer { Id = 1, QuestionId = 1, Correct = true, Content = "Model-View-Controller" },
				new Answer { Id = 1, QuestionId = 1, Correct = false, Content = "Model-Viewiór-Controll" },
				new Answer { Id = 1, QuestionId = 1, Correct = false, Content = "Mama-Voła-Colacja" },
				new Answer { Id = 1, QuestionId = 1, Correct = false, Content = "Żodyn z powyższych" },
				new Answer { Id = 1, QuestionId = 2, Correct = false, Content = "Nie wiem" },
				new Answer { Id = 1, QuestionId = 2, Correct = true, Content = "TempData służy do przechowywania danych tymczasowych" },
				new Answer { Id = 1, QuestionId = 2, Correct = true, Content = "ViewData służy do przechowywania danych dla widoku" },
				new Answer { Id = 1, QuestionId = 2, Correct = false, Content = "Żodyn z powyższych" },
			}.ForEach(i => context.Answer.Add(i));

			var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
			var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

			if (!RoleManager.RoleExists("admin"))
			{
				RoleManager.Create(new IdentityRole("admin"));
			}

			if (UserManager.FindByName("admin") == null)
			{
				PasswordHasher ps = new PasswordHasher();
				ApplicationUser admin = new ApplicationUser { UserName = "admin", EmailConfirmed = true, PasswordHash = ps.HashPassword("admin") };
				UserManager.Create(admin);
				if (!UserManager.IsInRole(admin.Id, "admin"))
					UserManager.AddToRole(admin.Id, "admin");
			}

			context.SaveChanges();
		}
	}

	public class DropCreateIfChangeInitializer : DropCreateDatabaseIfModelChanges<SystemEgzaminacyjnyDbContext>
    {
		protected override void Seed(SystemEgzaminacyjnyDbContext context)
        {
            context.Seed(context);

            base.Seed(context);
        }
    }

	public class ApplicationUser : IdentityUser
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string StudentGroup { get; set; }
	}
}