﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SystemEgzaminacyjny.Views.Shared.EditorTemplates
{
	public class SelectEnrollmentEditorViewModel
	{
		public int Id { get; set; }
		public int ExamId { get; set; }
		[Required]
		public string UserId { get; set; }
		public int Score { get; set; }
		[DataType(DataType.Date)]
		public DateTime? ExamDate { get; set; }
		[DataType(DataType.Time)]
		public DateTime? ExamTime { get; set; }
		public bool IsSelected { get; set; }
	}
}