﻿$(function () {
	$('[data-toggle="confirmation"]').confirmation(options)
	$('.datetimepicker').datetimepicker({
		locale: 'pl',
		format: 'YYYY-MM-DD HH:mm'
	})
	$('#selectAll').change(function () {
		var status = $('#selectAll').prop('checked');
		checkAll(status);
	})
	$('.delete-link').click(function () {
		return false;
	});
	$('[data-countdown]').each(function() {
		var $this = $(this), finalDate = new Date().setMilliseconds($(this).data('countdown'));
		$this.countdown(finalDate).on('update.countdown', function (event) {
			$this.text(event.strftime('%H:%M:%S'));
		}).on('finish.countdown', function (event) {
			window.location.href = '/Exam/EndExam/' + $this.data('enrollment');
		});
	});
})

var options = {
	onConfirm: removeObject
}

function removeObject() {

	var id = $(this).attr('id');
	$.ajax({
		url: "/Enrollment/Delete/",
		type: 'POST',
		data: { id: id }
	});

	var $tr = $(this).parents("tr:first");
	$tr.fadeOut(500);
	
	return false;
}

function checkAll(status) {
	$(".check-box").each(function () {
		$(this).prop("checked", status);
	});
}

function toggleEnroll() {
	var button = $(this).find("button");

	if (button.hasClass("btn-success"))
	{
		button.text("Wypisz się");
	}
	else
	{
		button.text("Zapisz się");
	}
	button.toggleClass("btn-success");
	button.toggleClass("btn-warning");
}

function toggleExamStart() {
	var button = $(this).find("button");

	if (button.hasClass("btn-default")) {
		button.text("Zakończ egzamin");
	}
	else {
		button.text("Rozpocznij egzamin");
	}
	button.toggleClass("btn-default");
	button.toggleClass("btn-danger");
}