﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SystemEgzaminacyjny.Service.Services.Interfaces;
using SystemEgzaminacyjny.Service.ViewModels.Answer;

namespace SystemEgzaminacyjny.Controllers
{
	[Authorize(Roles = "admin")]
    public class AnswerController : Controller
    {
		private readonly IAnswerService _answerService;

		public AnswerController(IAnswerService answerService)
		{
			_answerService = answerService;
		}
        // GET: Answer
        public ActionResult List(int questionId, int examId)
        {
			var model = new AnswerListViewModel()
			{
				Id = questionId,
				ExamId = examId,
				List = _answerService.GetByQuestionId(questionId)
			};

            return View(model);
        }

        // GET: Answer/Create
		public ActionResult Create(int questionId, int examId)
		{
			var model = new AnswerViewModel
			{
				QuestionId = questionId,
				ExamId = examId
			};

            return View(model);
        }

        // POST: Answer/Create
        [HttpPost]
        public ActionResult Create(AnswerViewModel model)
        {
            try
            {
				_answerService.Add(model);

				return RedirectToAction("List", new { questionId = model.QuestionId, examId = model.ExamId });
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Answer/Edit/5
        public ActionResult Edit(int id)
        {
            return View(_answerService.GetSingle(id));
        }

        // POST: Answer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, AnswerViewModel model)
        {
            try
			{
				_answerService.Update(model);

				return RedirectToAction("List", new { questionId = model.QuestionId, examId = model.ExamId });
            }
            catch
            {
                return View(model);
            }
        }

        // POST: Answer/Delete/5
		[HttpPost]
		public ActionResult Delete(int id)
		{
			_answerService.Delete(id);

			return Content(Boolean.TrueString);
		}
    }
}
