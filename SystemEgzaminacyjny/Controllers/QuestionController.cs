﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SystemEgzaminacyjny.Service.Services.Interfaces;
using SystemEgzaminacyjny.Service.ViewModels.Question;

namespace SystemEgzaminacyjny.Controllers
{
	[Authorize(Roles = "admin")]
    public class QuestionController : Controller
    {
		private readonly IQuestionService _questionService;

		public QuestionController(IQuestionService questionService)
		{
			_questionService = questionService;
		}

        // GET: Question
        public ActionResult List(int examId)
        {
			var model = new QuestionListViewModel
			{
				Id = examId,
				List = _questionService.GetByExamId(examId)
			};

            return View(model);
        }

        // GET: Question/Create
		public ActionResult Create(int examId)
        {
			var model = new QuestionViewModel
			{
				ExamId = examId
			};

            return View(model);
        }

        // POST: Question/Create
        [HttpPost]
        public ActionResult Create(QuestionViewModel model)
        {
            try
			{
				if (!ModelState.IsValid)
					return View(model);

				_questionService.Add(model);

				return RedirectToAction("List", new { examId = model.ExamId });
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Question/Edit/5
        public ActionResult Edit(int id, int examId)
        {
			return View(_questionService.GetSingle(id));
        }

        // POST: Question/Edit/5
        [HttpPost]
		public ActionResult Edit(int id, int examId, QuestionViewModel model)
        {
            try
            {
				_questionService.Update(model);

				return RedirectToAction("List", new { examId = model.ExamId });
            }
            catch
            {
                return View(model);
            }
        }

        // POST: Question/Delete/5
		[HttpPost]
		public ActionResult Delete(int id)
		{
			_questionService.Delete(id);

			return Content(Boolean.TrueString);
		}
    }
}
