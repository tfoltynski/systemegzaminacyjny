﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SystemEgzaminacyjny.Service.Services.Interfaces;
using SystemEgzaminacyjny.Service.ViewModels.Enrollment;
using Microsoft.AspNet.Identity;

namespace SystemEgzaminacyjny.Controllers
{
    public class EnrollmentController : Controller
    {
		private readonly IEnrollmentService _enrollmentService;
		private readonly IExamService _examService;

		public EnrollmentController(IEnrollmentService enrollmentService, IExamService examService)
		{
			_enrollmentService = enrollmentService;
			_examService = examService;
		}

        // GET: Enrollment
		[Authorize(Roles = "admin")]
        public ActionResult List(int examId)
        {
			var model = new EnrollmentListViewModel
			{
				ExamId = examId,
				ExamTopic = _examService.GetSingle(examId).Topic,
				List = _enrollmentService.GetByExamId(examId)
			};
			
            return View(model);
        }

		[Authorize(Roles = "admin")]
		public ActionResult ListWithoutDate(int examId)
		{
			var model = new EnrollmentListViewModel
			{
				ExamId = examId,
				ExamTopic = _examService.GetSingle(examId).Topic,
				List = _enrollmentService.GetWithoutDate(examId)
			};

			return View(model);
		}

		public ActionResult YourList()
		{
			var model = new EnrollmentListViewModel
			{
				List = _enrollmentService.GetByUserId(User.Identity.GetUserId())
			};

			return View(model);
		}

		public ActionResult Score(int id)
		{
			return View(_enrollmentService.GetScoreForEnrollment(id));
		}

        // GET: Enrollment/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Enrollment/Create
		//public ActionResult Create(int examId)
		//{
		//	var model = new EnrollmentCreateViewModel
		//	{
		//		ExamId = examId,
		//		UserId = User.Identity.GetUserId()
		//	};

		//	return View(model);
		//}

        // POST: Enrollment/Create
		[Authorize]
        [HttpPost]
        public ActionResult Create(int examId)
		{
			var userId = User.Identity.GetUserId();

			if (_enrollmentService.GetByExamId(examId).Count == 0)
			{
				_enrollmentService.Add(examId, userId);

				return Content(Boolean.TrueString);
			}
			var enrollment = _enrollmentService.GetUserEnrollmentForExam(userId, examId, false);

			if (enrollment == null)
			{
				_enrollmentService.Add(examId, userId);
			}
			else
			{
				_enrollmentService.Delete(enrollment.Id);
			}

			return Content(Boolean.TrueString);
        }

        // GET: Enrollment/Edit/5
		[Authorize(Roles = "admin")]
        public ActionResult Edit(int id)
        {
			return View(_enrollmentService.GetSingle(id));
        }

        // POST: Enrollment/Edit/5
		[Authorize(Roles = "admin")]
        [HttpPost]
		public ActionResult Edit(int id, EditEnrollmentViewModel model)
		{
			if(ModelState.IsValid)
			{
				_enrollmentService.UpdateExamDate(model);

				return RedirectToAction("List", new { examId = model.ExamId });
			}
			else
			{
				return View(model);
			}
        }

		// POST: Enrollment/Edit/5
		[Authorize(Roles = "admin")]
		[HttpPost]
		public ActionResult EditSelected(int examId, EnrollmentListViewModel model)
		{
			return View(model);
		}

		// POST: Enrollment/Edit/5
		[Authorize(Roles = "admin")]
		[HttpPost]
		public ActionResult EditSelected_Post(EnrollmentListViewModel model)
		{
			try
			{
				var list = model.List.FindAll(p => p.IsSelected);

				var newModel = new EnrollmentListViewModel
				{
					ExamId = model.List[0].ExamId,
					ExamDate = model.ExamDate,
					List = list
				};

				_enrollmentService.UpdateSelected(newModel);

				return RedirectToAction("List", new { examId = model.ExamId });
			}
			catch
			{
				return View("EditSelected", model);
			}
		}

        // POST: Enrollment/Delete/5
		[Authorize(Roles = "admin")]
		[HttpPost]
		public ActionResult Delete(int id)
		{
			_enrollmentService.Delete(id);
			return Content(Boolean.TrueString);
		}
    }
}
