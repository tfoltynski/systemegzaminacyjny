﻿using MvcSiteMapProvider.Web.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SystemEgzaminacyjny.Service.Services.Interfaces;
using SystemEgzaminacyjny.Service.ViewModels.Exam;
using Microsoft.AspNet.Identity;

namespace SystemEgzaminacyjny.Controllers
{
    public class ExamController : Controller
    {
		private readonly IExamService _examService;
		private readonly IEnrollmentService _enrollmentService;
		private readonly IQuestionService _questionService;

		public ExamController(IExamService examService, IEnrollmentService enrollmentService, IQuestionService questionService)
		{
			_examService = examService;
			_enrollmentService = enrollmentService;
			_questionService = questionService;
		}

        // GET: Exam
        public ActionResult List()
        {
			var examList = _examService.GetAll();
			foreach (var item in examList)
			{
				//item.IsExamInProgress = _examService.IsExamInProgress(item.Id);
				item.IsUserEnrolled = _enrollmentService.GetUserEnrollmentForExam(User.Identity.GetUserId(), item.Id, false) != null ? true : false;
			}

			var model = new ExamListViewModel()
			{
				List = examList
			};
			
            return View(model);
        }

		[Authorize(Roles="admin")]
		[HttpGet]
		public ActionResult StartExam(int id)
		{
			var model = new StartExamViewModel()
			{
				ExamId = id,
				NumberOfQuestionsInExam = _questionService.GetNumberOfQuestionsInExam(id)
			};

			return View(model);
		}

		[Authorize(Roles = "admin")]
		[HttpPost]
		public ActionResult StartExam(StartExamViewModel model)
		{
			_examService.StartExam(model);

			return RedirectToAction("List");
		}

		[Authorize]
		public ActionResult EndExam(int id)
		{
			_examService.EndExam(id, User.Identity.GetUserId());

			return RedirectToAction("EndOfExam");
		}

		public ActionResult EndOfExam()
		{
			return View();
		}

		[Authorize]
		[HttpGet]
		public ActionResult GetQuestion(int id)
		{
			var question = _examService.GetQuestion(id);

			if (question == null || question.RemainingTime < DateTime.MinValue.Ticks)
			{
				_examService.EndExam(id, User.Identity.GetUserId());
				return RedirectToAction("EndOfExam");
			}

			return View(question);
		}

		[Authorize]
		[HttpPost]
		public ActionResult	SaveUserAnswer(GetQuestionViewModel model)
		{
			_examService.SaveUserAnswer(model.Id, model);

			return RedirectToAction("GetQuestion", new { id = model.Id });
		}

        // GET: Exam/Details/5
        public ActionResult Details(int id)
        {
			return View(_examService.GetSingle(id));
        }

        // GET: Exam/Create
		[Authorize(Roles="admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Exam/Create
		[Authorize(Roles = "admin")]
		[HttpPost]
		[ValidateAntiForgeryToken]
        public ActionResult Create(ExamViewModel model)
        {
            try
            {
				if (!ModelState.IsValid)
					return View(model);

				_examService.Add(model);

                return RedirectToAction("List");
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Exam/Edit/5
		[Authorize(Roles = "admin")]
        public ActionResult Edit(int id)
        {
			return View(_examService.GetSingle(id));
        }

        // POST: Exam/Edit/5
		[Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult Edit(int id, ExamViewModel model)
        {
            try
            {
				_examService.Update(model);

                return RedirectToAction("List");
            }
            catch
            {
                return View(model);
            }
        }

        // POST: Exam/Delete/5
		[Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult Delete(int id)
        {
			_examService.Delete(id);

			return RedirectToAction("List");
        }
    }
}
