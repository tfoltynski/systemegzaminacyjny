﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SystemEgzaminacyjny.Startup))]
namespace SystemEgzaminacyjny
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
			ConfigureAuth(app);
        }
    }
}
