﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SystemEgzaminacyjny
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				name: "Enrollment",
				url: "Enrollment/{action}/{examId}/{id}",
				defaults: new { controller = "Enrollment", action = "List", examId = UrlParameter.Optional, id = UrlParameter.Optional },
				constraints: new { controller = @"[Ee]nrollment" });

			routes.MapRoute(
				name: "Question",
				url: "Question/{action}/{examId}/{id}",
				defaults: new { controller = "Question", action = "List", examId = UrlParameter.Optional, id = UrlParameter.Optional },
				constraints: new { controller = @"[Qq]uestion" });

			routes.MapRoute(
				name: "Answer",
				url: "Answer/{action}/{examId}/{questionId}/{id}",
				defaults: new { controller = "Answer", action = "List", id = UrlParameter.Optional },
				constraints: new { controller = @"[Aa]nswer" });

			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "List", id = UrlParameter.Optional }
			);



		}
	}
}
