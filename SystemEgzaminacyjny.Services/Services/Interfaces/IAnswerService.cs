﻿using System.Collections.Generic;
using SystemEgzaminacyjny.Service.ViewModels.Answer;

namespace SystemEgzaminacyjny.Service.Services.Interfaces
{
	public interface IAnswerService
	{
		AnswerViewModel GetSingle(int id);
		List<AnswerViewModel> GetAll();
		List<AnswerViewModel> GetByQuestionId(int id);
		List<AnswerViewModel> GetByContent(string content);
		void Add(AnswerViewModel exam);
		void Update(AnswerViewModel exam);
		void Delete(int id);
	}
}
