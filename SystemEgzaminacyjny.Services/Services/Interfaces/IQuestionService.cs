﻿using System.Collections.Generic;
using SystemEgzaminacyjny.Service.ViewModels.Question;

namespace SystemEgzaminacyjny.Service.Services.Interfaces
{
	public interface IQuestionService
	{
		QuestionViewModel GetSingle(int id);
		List<QuestionViewModel> GetAll();
		List<QuestionViewModel> GetByExamId(int id);
		List<QuestionViewModel> GetByContent(string content);
		int GetNumberOfQuestionsInExam(int id);
		void Add(QuestionViewModel question);
		void Update(QuestionViewModel question);
		void Delete(int id);
	}
}
