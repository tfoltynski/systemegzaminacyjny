﻿using System;
using System.Collections.Generic;
using SystemEgzaminacyjny.Service.ViewModels.Exam;

namespace SystemEgzaminacyjny.Service.Services.Interfaces
{
	public interface IExamService
	{
		ExamViewModel GetSingle(int id);
		List<ExamViewModel> GetAll();
		List<ExamViewModel> GetByTopic(string topic);
		bool IsExamInProgress(int examId);
		void StartExam(StartExamViewModel model);
		void EndExam(int enrollmentId, string userId);
		GetQuestionViewModel GetQuestion(int enrollmentId);
		void SaveUserAnswer(int enrollmentId, GetQuestionViewModel model);
		long GetRemainingTime(int id);
		void Add(ExamViewModel exam);
		void Update(ExamViewModel exam);
		void Delete(int id);
	}
}
