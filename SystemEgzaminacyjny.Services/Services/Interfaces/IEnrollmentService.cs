﻿using System.Collections.Generic;
using SystemEgzaminacyjny.Service.ViewModels.Enrollment;

namespace SystemEgzaminacyjny.Service.Services.Interfaces
{
	public interface IEnrollmentService
	{
		EditEnrollmentViewModel GetSingle(int id);
		List<EnrollmentViewModel> GetAll();
		List<EnrollmentViewModel> GetByExamId(int id);
		List<EnrollmentViewModel> GetByUserId(string id);
		List<EnrollmentViewModel> GetWithoutDate(int id);
		ScoreViewModel GetScoreForEnrollment(int enrollmentId);
		EnrollmentViewModel GetUserEnrollmentForExam(string id, int examId, bool isFinished);
		void Add(int examId, string userId);
		void UpdateExamDate(EditEnrollmentViewModel enrollment);
		void UpdateSelected(EnrollmentListViewModel enrollments);
		void Delete(int id);
	}
}
