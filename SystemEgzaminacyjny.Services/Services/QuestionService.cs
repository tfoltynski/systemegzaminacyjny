﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SystemEgzaminacyjny.DAL.Entities;
using SystemEgzaminacyjny.DAL.Repositories.Interfaces;
using SystemEgzaminacyjny.Service.Services.Interfaces;
using SystemEgzaminacyjny.Service.ViewModels.Question;

namespace SystemEgzaminacyjny.Service.Services
{
	public class QuestionService : IQuestionService
	{
		private readonly IQuestionRepository _questionRepository;

		public QuestionService(IQuestionRepository questionRepository)
		{
			_questionRepository = questionRepository;
		}

		public QuestionViewModel GetSingle(int id)
		{
			var question = _questionRepository.GetSingle(p => p.Id == id);

			if (question == null)
				return null;

			return FillObjectProperties(question);
		}

		public List<QuestionViewModel> GetAll()
		{
			return _questionRepository.GetAll().Select(p => new QuestionViewModel()
			{
				Id = p.Id,
				ExamId = p.ExamId,
				Content = p.Content,
				ImagePath = p.ImagePath
			}).ToList();
		}

		public List<QuestionViewModel> GetByExamId(int id)
		{
			return _questionRepository.FindBy(p => p.ExamId == id).Select(p => new QuestionViewModel()
				{
					Id = p.Id,
					ExamId = p.ExamId,
					Content = p.Content,
					ImagePath = p.ImagePath
				}).ToList();
		}

		public List<QuestionViewModel> GetByContent(string content)
		{
			return _questionRepository.FindBy(p => p.Content == content).Select(p => new QuestionViewModel()
			{
				Id = p.Id,
				ExamId = p.ExamId,
				Content = p.Content,
				ImagePath = p.ImagePath
			}).ToList();
		}

		public int GetNumberOfQuestionsInExam(int id)
		{
			return _questionRepository.FindBy(p => p.ExamId == id).Count();
		}

		public void Add(QuestionViewModel question)
		{
			_questionRepository.Add(new Question() { ExamId = question.ExamId, Content = question.Content, ImagePath = question.ImagePath });
			_questionRepository.Save();
		}

		public void Update(QuestionViewModel question)
		{
			_questionRepository.Update(new Question() { Id = question.Id, ExamId = question.ExamId, Content = question.Content, ImagePath = question.ImagePath });
			_questionRepository.Save();
		}

		public void Delete(int id)
		{
			_questionRepository.Delete(_questionRepository.GetSingle(p => p.Id == id));
			_questionRepository.Save();
		}

		private QuestionViewModel FillObjectProperties(Question question)
		{
			return new QuestionViewModel()
			{
				Id = question.Id,
				ExamId = question.ExamId,
				Content = question.Content,
				ImagePath = question.ImagePath
			};
		}
	}
}