﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using SystemEgzaminacyjny.DAL.Entities;
using SystemEgzaminacyjny.DAL.Repositories.Interfaces;
using SystemEgzaminacyjny.Service.Services.Interfaces;
using SystemEgzaminacyjny.Service.ViewModels.Answer;

namespace SystemEgzaminacyjny.Service.Services
{
	public class AnswerService : IAnswerService
	{
		private readonly IAnswerRepository _answerRepository;

		public AnswerService(IAnswerRepository answerRepository)
		{
			_answerRepository = answerRepository;
		}

		public AnswerViewModel GetSingle(int id)
		{
			var answer = _answerRepository.GetSingle(p => p.Id == id);

			if (answer == null)
				return null;

			return FillObjectProperties(answer);
		}

		public List<AnswerViewModel> GetAll()
		{
			return _answerRepository.GetAll().Select(p => new AnswerViewModel()
			{
				Id = p.Id,
				QuestionId = p.QuestionId,
				Content = p.Content,
				Correct = p.Correct
			}).ToList();
		}

		public List<AnswerViewModel> GetByQuestionId(int id)
		{
			return _answerRepository.FindBy(p => p.QuestionId == id).Select(p => new AnswerViewModel()
			{
				Id = p.Id,
				ExamId = p.Question.ExamId,
				QuestionId = p.QuestionId,
				Content = p.Content,
				Correct = p.Correct
			}).ToList();
		}

		public List<AnswerViewModel> GetByContent(string content)
		{
			return _answerRepository.FindBy(p => p.Content == content).Select(p => new AnswerViewModel()
			{
				Id = p.Id,
				QuestionId = p.QuestionId,
				Content = p.Content,
				Correct = p.Correct
			}).ToList();
		}

		public void Add(AnswerViewModel answer)
		{
			_answerRepository.Add(new Answer() { QuestionId = answer.QuestionId, Content = answer.Content, Correct = answer.Correct });
			_answerRepository.Save();
		}

		public void Update(AnswerViewModel answer)
		{
			_answerRepository.Update(new Answer() { Id = answer.Id, QuestionId = answer.QuestionId, Content = answer.Content, Correct = answer.Correct });
			_answerRepository.Save();
		}

		public void Delete(int id)
		{
			_answerRepository.Delete(_answerRepository.GetSingle(p => p.Id == id));
			_answerRepository.Save();
		}

		private AnswerViewModel FillObjectProperties(Answer answer)
		{
			return new AnswerViewModel()
			{
				Id = answer.Id,
				ExamId = answer.Question.ExamId,
				QuestionId = answer.QuestionId,
				Content = answer.Content,
				Correct = answer.Correct
			};
		}
	}
}