﻿using System.Collections.Generic;
using System.Linq;
using SystemEgzaminacyjny.DAL.Entities;
using SystemEgzaminacyjny.DAL.Repositories.Interfaces;
using SystemEgzaminacyjny.Service.Services.Interfaces;
using SystemEgzaminacyjny.Service.ViewModels.Enrollment;
using System;

namespace SystemEgzaminacyjny.Service.Services
{
	public class EnrollmentService : IEnrollmentService
	{
		private readonly IEnrollmentRepository _enrollmentRepository;
		private readonly IQuestionRepository _questionRepository;

		public EnrollmentService(IEnrollmentRepository enrollmentRepository, IQuestionRepository questionRepository)
		{
			_enrollmentRepository = enrollmentRepository;
			_questionRepository = questionRepository;
		}

		public EditEnrollmentViewModel GetSingle(int id)
		{
			var enrollment = _enrollmentRepository.GetSingle(p => p.Id == id);

			if (enrollment == null)
				return null;

			var viewModel = new EditEnrollmentViewModel
			{
				Id = enrollment.Id,
				ExamId = enrollment.ExamId,
				ExamDate = enrollment.ExamDate
			};

			return viewModel;
		}

		public List<EnrollmentViewModel> GetAll()
		{
			return _enrollmentRepository.GetAll().Select(p => new EnrollmentViewModel()
			{
				Id = p.Id,
				ExamId = p.ExamId,
				UserId = p.UserId,
				Score = p.Score,
				ExamDate = p.ExamDate,
				EnrollDate = p.EnrollDate,
				IsExamFinished = p.IsExamFinished,
				IsExamInProgress = p.IsExamInProgress,
				NumberOfQuestions = p.NumberOfQuestions,
				PassThreshold = p.PassThreshold,
			}).ToList();
		}

		public List<EnrollmentViewModel> GetByExamId(int id)
		{
			return _enrollmentRepository.FindBy(p => p.ExamId == id && p.IsExamFinished == false).Select(p => new EnrollmentViewModel()
			{
				Id = p.Id,
				ExamId = p.ExamId,
				UserId = p.UserId,
				Score = p.Score,
				ExamDate = p.ExamDate,
				EnrollDate = p.EnrollDate,
				IsExamFinished = p.IsExamFinished,
				IsExamInProgress = p.IsExamInProgress,
				NumberOfQuestions = p.NumberOfQuestions,
				PassThreshold = p.PassThreshold,
				User = p.User
			}).ToList();
		}

		public List<EnrollmentViewModel> GetByUserId(string id)
		{
			return _enrollmentRepository.FindBy(p => p.UserId == id).Select(p => new EnrollmentViewModel()
			{
				Id = p.Id,
				ExamId = p.ExamId,
				UserId = p.UserId,
				Score = p.Score,
				ExamDate = p.ExamDate,
				EnrollDate = p.EnrollDate,
				IsExamFinished = p.IsExamFinished,
				IsExamInProgress = p.IsExamInProgress,
				NumberOfQuestions = p.NumberOfQuestions,
				PassThreshold = p.PassThreshold,
				Exam = p.Exam,
				User = p.User
			}).ToList();
		}

		public List<EnrollmentViewModel> GetWithoutDate(int id)
		{
			return _enrollmentRepository.FindBy(p => p.ExamId == id && p.ExamDate == null).Select(p => new EnrollmentViewModel()
			{
				Id = p.Id,
				ExamId = p.ExamId,
				UserId = p.UserId,
				Score = p.Score
			}).ToList();
		}

		public ScoreViewModel GetScoreForEnrollment(int enrollmentId)
		{
			var enrollment = _enrollmentRepository.GetSingle(p => p.Id == enrollmentId);

			if (enrollment == null)
				return null;

			var viewModel = new ScoreViewModel
			{
				Id = enrollmentId,
				Score = Math.Round((double)enrollment.Score, 1).ToString("0.0"),
				PassThreshold = enrollment.PassThreshold,
				IsPassed = (enrollment.Score >= enrollment.PassThreshold) ? true : false
			};

			return viewModel;
		}

		public EnrollmentViewModel GetUserEnrollmentForExam(string id, int examId, bool isFinished)
		{
			var enrollment = _enrollmentRepository.GetSingle(p => p.UserId == id && p.ExamId == examId && p.IsExamFinished == isFinished);

			if (enrollment == null)
				return null;

			return FillObjectProperties(enrollment);
		}

		public void Add(int examId, string userId)
		{
			_enrollmentRepository.Add(new Enrollment() { ExamId = examId, UserId = userId, EnrollDate = DateTime.Now });
			_enrollmentRepository.Save();
		}

		public void UpdateExamDate(EditEnrollmentViewModel enrollment)
		{
			_enrollmentRepository.Update(enrollment.Id, enrollment.ExamDate);
		}

		public void UpdateSelected(EnrollmentListViewModel enrollments)
		{
			foreach (var item in enrollments.List)
			{
				_enrollmentRepository.Update(item.Id, enrollments.ExamDate);
			}
		}

		public void Delete(int id)
		{
			_enrollmentRepository.Delete(_enrollmentRepository.GetSingle(p => p.Id == id));
			_enrollmentRepository.Save();
		}

		private EnrollmentViewModel FillObjectProperties(Enrollment enrollment)
		{
			return new EnrollmentViewModel()
			{
				Id = enrollment.Id,
				ExamId = enrollment.ExamId,
				UserId = enrollment.UserId,
				Score = enrollment.Score,
				ExamDate = enrollment.ExamDate,
				EnrollDate = enrollment.EnrollDate,
				IsExamFinished = enrollment.IsExamFinished,
				IsExamInProgress = enrollment.IsExamInProgress,
				NumberOfQuestions = enrollment.NumberOfQuestions,
				PassThreshold = enrollment.PassThreshold,
				Exam = enrollment.Exam,
				User = enrollment.User
			};
		}
	}
}