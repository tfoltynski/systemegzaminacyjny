﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SystemEgzaminacyjny.DAL.Entities;
using SystemEgzaminacyjny.DAL.Repositories.Interfaces;
using SystemEgzaminacyjny.Service.ViewModels.Exam;
using SystemEgzaminacyjny.Service.Services.Interfaces;
using System.Linq.Expressions;
using SystemEgzaminacyjny.Service.ViewModels.Enrollment;

namespace SystemEgzaminacyjny.Service.Services
{
	public class ExamService : IExamService
	{
		private readonly IExamRepository _examRepository;
		private readonly IEnrollmentRepository _enrollmentRepository;
		private readonly IEnrollmentService _enrollmentService;
		private readonly IQuestionRepository _questionRepository;
		private readonly IRandomQuestionRepository _randomQuestionRepository;
		private readonly IAnswerRepository _answerRepository;
		private readonly IUserAnswerRepository _userAnswerRepository;

		public ExamService(IExamRepository examRepository, IEnrollmentRepository enrollmentRepository, IEnrollmentService enrollmentService, IQuestionRepository questionRepository,
			IRandomQuestionRepository randomQuestionRepository, IAnswerRepository answerRepository, IUserAnswerRepository userAnswerRepository)
		{
			_examRepository = examRepository;
			_enrollmentRepository = enrollmentRepository;
			_enrollmentService = enrollmentService;
			_questionRepository = questionRepository;
			_randomQuestionRepository = randomQuestionRepository;
			_answerRepository = answerRepository;
			_userAnswerRepository = userAnswerRepository;
		}

		public ExamViewModel GetSingle(int id)
		{
			var exam = _examRepository.GetSingle(p => p.Id == id);

			if (exam == null)
				return null;

			return FillObjectProperties(exam);
		}

		public List<ExamViewModel> GetAll()
		{
			return _examRepository.GetAll().Select(p => new ExamViewModel()
			{
				Id = p.Id,
				Duration = p.Duration,
				Topic = p.Topic,
				Enrollments = p.Enrollments
			}).ToList();
		}

		public List<ExamViewModel> GetByTopic(string topic)
		{
			return _examRepository.FindBy(p => p.Topic == topic).Select(p => new ExamViewModel()
			{
				Id = p.Id,
				Topic = p.Topic,
				Duration = p.Duration,
				Enrollments = p.Enrollments
			}).ToList();
		}

		public bool IsExamInProgress(int examId)
		{
			return _enrollmentRepository.FindBy(p => p.ExamId == examId && p.IsExamInProgress == true) != null ? true : false;
		}

		public void StartExam(StartExamViewModel model)
		{
			Random rand = new Random();
			var enrollments = GetClosestEnrollmentsToDate(model.ExamId);
			var allQuestions = _questionRepository.GetByExamId(model.ExamId);

			foreach (var enroll in enrollments)
			{
				var allQuestionsTemp = allQuestions;

				for (int i = 0; i < model.NumberOfQuestions; i++)
				{
					int randomQuestion = rand.Next(allQuestionsTemp.Count);

					_randomQuestionRepository.Add(new RandomQuestion() { QuestionId = allQuestionsTemp[randomQuestion].Id, EnrollmentId = enroll.Id, IsAnswered = false });
					allQuestionsTemp.RemoveAt(randomQuestion);
				}

				enroll.IsExamInProgress = true;
				enroll.NumberOfQuestions = model.NumberOfQuestions;
				enroll.PassThreshold = model.PassThreshold;
				enroll.ExamDate = DateTime.Now;
			}

			_enrollmentRepository.Save();
			_randomQuestionRepository.Save();
		}

		public void EndExam(int enrollmentId, string userId)
		{
			var enrollment = _enrollmentRepository.GetSingle(p => p.Id == enrollmentId);

			if (enrollment.UserId == userId)
			{
				enrollment.IsExamInProgress = false;
				enrollment.IsExamFinished = true;

				var randomQuestions = enrollment.RandomQuestions.ToList();
				var questions = _questionRepository.GetByExamId(enrollment.ExamId).ToList();
				bool correctAnswer = false;
				float score = 0;

				foreach (var currentQuestion in randomQuestions)
				{
					var question = questions.FirstOrDefault(p => p.Id == currentQuestion.QuestionId);

					if (currentQuestion.UserAnswers.Count == 0)
						break;

					for (int i = 0; i < question.Answers.Count; i++)
					{
						if (question.Answers.ElementAt(i).Correct == currentQuestion.UserAnswers.ElementAt(i).IsSelected)
						{
							correctAnswer = true;
						}
						else
						{
							correctAnswer = false;
							break;
						}
					}
					if (correctAnswer == true)
					{
						score++;
					}
				}
				enrollment.Score = (score / enrollment.NumberOfQuestions) * 100;

				_enrollmentRepository.Save();
			}
		}

		public GetQuestionViewModel GetQuestion(int enrollmentId)
		{
			var question = _randomQuestionRepository.GetQuestionForEnrollment(enrollmentId);

			if (question == null)
				return null;

			var model = new GetQuestionViewModel()
			{
				Id = enrollmentId,
				QuestionId = question.Id,
				Question = _questionRepository.GetSingle(p => p.Id == question.QuestionId),
				Answers = _answerRepository.GetByQuestionId(question.QuestionId),
				RemainingTime = GetRemainingTime(enrollmentId),
				RemainingQuestions = _randomQuestionRepository.RemainingQuestions(enrollmentId)
			};

			return model;
		}

		public void SaveUserAnswer(int enrollmentId, GetQuestionViewModel model)
		{
			_randomQuestionRepository.GetSingle(p => p.Id == model.QuestionId).IsAnswered = true;

			for (int i = 0; i < model.Answers.Count; i++)
			{
				_userAnswerRepository.Add(new UserAnswer()
				{
					AnswerId = model.Answers[i].Id,
					IsSelected = model.UserAnswers[i],
					RandomQuestionId = model.QuestionId
				});
			}
			_randomQuestionRepository.Save();
			_userAnswerRepository.Save();
		}

		public long GetRemainingTime(int id)
		{
			var enrollment = _enrollmentRepository.GetSingle(p => p.Id == id);
			var duration = enrollment.Exam.Duration;
			TimeSpan remainingTime = new TimeSpan(0, duration, 0);
			remainingTime = remainingTime + ((DateTime)enrollment.ExamDate - DateTime.Now);

			return remainingTime.Ticks / TimeSpan.TicksPerMillisecond;
		}

		public void Add(ExamViewModel exam)
		{
			_examRepository.Add(new Exam() { Topic = exam.Topic, Duration = exam.Duration });
			_examRepository.Save();
		}

		public void Update(ExamViewModel exam)
		{
			_examRepository.Update(new Exam() { Id = exam.Id, Topic = exam.Topic, Duration = exam.Duration });
			_examRepository.Save();
		}

		public void Delete(int id)
		{
			_examRepository.Delete(_examRepository.GetSingle(p => p.Id == id));
			_examRepository.Save();
		}

		private ExamViewModel FillObjectProperties(Exam exam)
		{
			return new ExamViewModel()
			{
				Id = exam.Id,
				Topic = exam.Topic,
				Duration = exam.Duration,
				Enrollments = exam.Enrollments
			};
		}

		private List<Enrollment> GetClosestEnrollmentsToDate(int examId)
		{
			var enrollments = _enrollmentRepository.FindBy(p => p.ExamId == examId && p.IsExamFinished == false).Select(p => p.ExamDate).Distinct().ToList();
			DateTime now = DateTime.Now;
			TimeSpan? diff = TimeSpan.MaxValue;
			DateTime? enrollDate = DateTime.MaxValue;

			foreach (var enroll in enrollments)
			{
				var temp = enroll - now;

				if (temp < diff)
				{
					diff = temp;
					enrollDate = enroll;
				}
			}

			return _enrollmentRepository.FindBy(p => p.ExamDate == enrollDate).ToList();
		}
	}
}