﻿using System;
using System.ComponentModel.DataAnnotations;
using SystemEgzaminacyjny.DAL.DbModel;
using System.Collections.Generic;

namespace SystemEgzaminacyjny.Service.ViewModels.Enrollment
{
	public class EnrollmentViewModel
	{
		public int Id { get; set; }
		public int ExamId { get; set; }
		[Required]
		public string UserId { get; set; }
		[DataType(DataType.DateTime)]
		public DateTime? ExamDate { get; set; }
		public DateTime EnrollDate { get; set; }
		public bool IsExamFinished { get; set; }
		public bool IsExamInProgress { get; set; }
		public bool IsSelected { get; set; }
		public float Score { get; set; }
		public int NumberOfQuestions { get; set; }
		public int PassThreshold { get; set; }

		public DAL.Entities.Exam Exam { get; set; }
		public ApplicationUser User { get; set; }

		public string ExamDateString
		{
			get
			{
				return ExamDate.HasValue ? ExamDate.Value.ToString("yyyy-MM-dd HH:mm") : "Nie ustalono daty";
			}
		}
	}
}