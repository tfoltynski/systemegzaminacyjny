﻿using System;
using System.ComponentModel.DataAnnotations;
using SystemEgzaminacyjny.DAL.DbModel;
using System.Collections.Generic;

namespace SystemEgzaminacyjny.Service.ViewModels.Enrollment
{
	public class EditEnrollmentViewModel
	{
		public int Id { get; set; }
		public int ExamId { get; set; }
		[DataType(DataType.DateTime)]
		public DateTime? ExamDate { get; set; }
	}
}