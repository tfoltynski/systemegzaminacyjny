﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SystemEgzaminacyjny.Service.ViewModels.Enrollment
{
	public class EnrollmentListViewModel
	{
		public EnrollmentListViewModel()
		{
			List = new List<EnrollmentViewModel>();
		}

		public int ExamId { get; set; }
		public string ExamTopic { get; set; }
		[DataType(DataType.DateTime)]
		public DateTime? ExamDate { get; set; }
		public List<EnrollmentViewModel> List { get; set; }
	}
}