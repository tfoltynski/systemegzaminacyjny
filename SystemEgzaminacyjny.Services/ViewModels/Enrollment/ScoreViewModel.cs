﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SystemEgzaminacyjny.DAL.DbModel;

namespace SystemEgzaminacyjny.Service.ViewModels.Enrollment
{
	public class ScoreViewModel
	{
		public int Id { get; set; }
		public string Score { get; set; }
		public int PassThreshold { get; set; }
		public bool IsPassed { get; set; }
	}
}