﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemEgzaminacyjny.Service.ViewModels.Exam
{
	public class GetQuestionViewModel
	{
		public int Id { get; set; }
		public int QuestionId { get; set; }
		public bool IsAnswered { get; set; }
		public long RemainingTime { get; set; }
		public int RemainingQuestions { get; set; }

		public DAL.Entities.Question Question { get; set; }
		public List<DAL.Entities.Answer> Answers { get; set; }
		public List<bool> UserAnswers { get; set; }
	}
}