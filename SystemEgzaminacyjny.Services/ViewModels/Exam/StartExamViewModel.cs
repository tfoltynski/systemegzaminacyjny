﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SystemEgzaminacyjny.Service.ViewModels.Exam
{
	public class StartExamViewModel
	{
		public int ExamId { get; set; }
		public int NumberOfQuestionsInExam { get; set; }
		// To do: ograniczyć ilość pytań przez ilość pytań ogółem
		public int NumberOfQuestions { get; set; }
		[Range(0,100)]
		public int PassThreshold { get; set; }
	}
}