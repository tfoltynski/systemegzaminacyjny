﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemEgzaminacyjny.Service.ViewModels.Exam
{
	public class ExamListViewModel
	{
		public List<ExamViewModel> List { get; set; }
	}
}