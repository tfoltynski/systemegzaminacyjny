﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SystemEgzaminacyjny.Service.ViewModels.Exam
{
	public class ExamViewModel
	{
		public int Id { get; set; }
		public int Duration { get; set; }
		public string Topic { get; set; }
		public bool IsUserEnrolled { get; set; }

		public ICollection<SystemEgzaminacyjny.DAL.Entities.Enrollment> Enrollments { get; set; }
	}
}
