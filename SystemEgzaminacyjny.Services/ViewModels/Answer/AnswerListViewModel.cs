﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemEgzaminacyjny.Service.ViewModels.Answer
{
	public class AnswerListViewModel
	{
		public int Id { get; set; }
		public int ExamId { get; set; }
		public List<AnswerViewModel> List { get; set; }
	}
}