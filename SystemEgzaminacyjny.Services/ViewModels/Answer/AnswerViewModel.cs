﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemEgzaminacyjny.Service.ViewModels.Answer
{
	public class AnswerViewModel
	{
		public int Id { get; set; }
		public int ExamId { get; set; }
		public int QuestionId { get; set; }
		public string Content { get; set; }
		public bool Correct { get; set; }
	}
}