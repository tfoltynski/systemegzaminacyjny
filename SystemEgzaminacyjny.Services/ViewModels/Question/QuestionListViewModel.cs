﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemEgzaminacyjny.Service.ViewModels.Question
{
	public class QuestionListViewModel
	{
		public int Id { get; set; }
		public List<QuestionViewModel> List { get; set; }
	}
}