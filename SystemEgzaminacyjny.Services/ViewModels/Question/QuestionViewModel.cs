﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SystemEgzaminacyjny.Service.ViewModels.Answer;

namespace SystemEgzaminacyjny.Service.ViewModels.Question
{
	public class QuestionViewModel
	{
		public int Id { get; set; }
		public int ExamId { get; set; }
		public string Content { get; set; }
		public string ImagePath { get; set; }

		public List<AnswerViewModel> Answers { get; set; }
	}
}